﻿using System.Threading.Tasks;

namespace Xiex.Homework.Smapiot.Services
{
    public interface IPriceRegistry
    {
        Task<double> GetPrice(string service, int count);
    }
}