﻿using System.Threading.Tasks;

namespace Xiex.Homework.Smapiot.Services
{
    public interface ISubscriptionResolver
    {
        Task<string[]> GetSubscriptions(string userID);
    }
}