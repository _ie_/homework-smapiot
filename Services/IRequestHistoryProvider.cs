﻿using System;
using System.Threading.Tasks;

namespace Xiex.Homework.Smapiot.Services
{
    public interface IRequestHistoryProvider
    {
        Task<HistoryRecord[]> GetData(int year, int month, string subscriptionID);
    }

    public class HistoryData
    {
        public HistoryRecord[] Requests { get; set; }
    }

    public class HistoryRecord
    {
        public string ID { get; set; }
        public DateTime Requested { get; set; }
        public TimeSpan Duration { get; set; }
        public string ServiceName { get; set; }
        public string SubscriptionID { get; set; }
        public string OriginIP { get; set; }
        public string Target { get; set; }
    }
}