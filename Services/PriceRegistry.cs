﻿using System;
using System.Threading.Tasks;

namespace Xiex.Homework.Smapiot.Services
{
    public class PriceRegistry : IPriceRegistry
    {
        public Task<double> GetPrice(string service, int count)
        {
            var price = service.Length * count / Math.Log10(10 + count);
            return Task.FromResult(price);
        }
    }
}