﻿using System.Threading.Tasks;

namespace Xiex.Homework.Smapiot.Services
{
    public class SubscriptionResolver : ISubscriptionResolver
    {
        public Task<string[]> GetSubscriptions(string userID) => Task.FromResult(new[] {userID});
    }
}
