﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Serilog;

namespace Xiex.Homework.Smapiot.Services
{
    public class RequestHistoryProvider : IRequestHistoryProvider
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly string _baseURI;
        private readonly string _accessKey;
        private readonly JsonSerializer _serializer;

        private static readonly ConcurrentDictionary<(int year, int month), HistoryRecord[]> _cache =
            new ConcurrentDictionary<(int year, int month), HistoryRecord[]>();

        public RequestHistoryProvider(IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
            _baseURI = configuration["endpoints:requestHistory:baseURI"];
            _accessKey = configuration["endpoints:requestHistory:accessKey"];
            _serializer = JsonSerializer.Create();
        }

        public async Task<HistoryRecord[]> GetData(int year, int month, string subscriptionID)
        {
            var canCache = DateTime.UtcNow.Year > year || (DateTime.UtcNow.Year == year && DateTime.UtcNow.Month > month);
            var cacheKey = (year, month);

            if (canCache && _cache.TryGetValue(cacheKey, out var data))
                return data.Where(x => x.SubscriptionID == subscriptionID).ToArray();

            using var client = _clientFactory.CreateClient();
            var result = await client.GetAsync($"{_baseURI}/{year}/{month}?code={_accessKey}");

            if (!result.IsSuccessStatusCode)
            {
                Log.Error("Failed to request requests history. Details: StatusCode = {0}; Content: {1}", 
                    result.StatusCode, await result.Content.ReadAsStringAsync());
                throw new Exception("Requests history data is not available");
            }

            var rawContent = await result.Content.ReadAsStringAsync();
            var parseData = _serializer.Deserialize<HistoryData>(new JsonTextReader(new StringReader(rawContent)));
            if (canCache)
                _cache.TryAdd(cacheKey, parseData.Requests);
            
            return parseData.Requests.Where(x => x.SubscriptionID == subscriptionID).ToArray();
        }
    }
}