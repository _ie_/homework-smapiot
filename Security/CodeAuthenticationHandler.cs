﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Serilog;

namespace Xiex.Homework.Smapiot.Security
{
    // NOTE: a la security. do not try this at home. just for demo purposes
    public class CodeAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        public CodeAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock)
            : base(options, logger, encoder, clock)
        { }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            try
            {
                // in our FAKE authentication we take user ID from query parameter 'code'
                if (Request.Query["code"] is { } values && values.FirstOrDefault() is { } code)
                {
                    // let's base64 decode from code user ID
                    var bytes = Convert.FromBase64String(code);
                    var id = Encoding.UTF8.GetString(bytes);
                
                    var claims = new[] {
                        new Claim(ClaimTypes.NameIdentifier, id),
                    };
                    var identity = new ClaimsIdentity(claims, Scheme.Name);
                    var principal = new ClaimsPrincipal(identity);
                    var ticket = new AuthenticationTicket(principal, Scheme.Name);

                    return AuthenticateResult.Success(ticket);
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex,"Exception while authenticating request");
            }

            return AuthenticateResult.Fail("Missing Authorization Code");
        }
    }
}
