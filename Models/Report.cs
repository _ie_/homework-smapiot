﻿namespace Xiex.Homework.Smapiot.Models
{
    public class Report
    {
        public string SubscriptionID { get; set; }
        public Period Period { get; set; }
        public int TotalRequests { get; set; }
        public double TotalPrice { get; set; }
        public PerService[] TotalPerService { get; set; }
        public double? Estimation { get; set; }
    }
}