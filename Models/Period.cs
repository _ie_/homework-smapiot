﻿using System;

namespace Xiex.Homework.Smapiot.Models
{
    public class Period
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
    }
}