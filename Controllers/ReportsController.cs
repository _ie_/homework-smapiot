﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Xiex.Homework.Smapiot.Models;
using Xiex.Homework.Smapiot.Services;

namespace Xiex.Homework.Smapiot.Controllers
{
    [Route("api/[controller]")]
    public class ReportsController : ControllerBase
    {
        private readonly ISubscriptionResolver _subscriptionResolver;
        private readonly IRequestHistoryProvider _requestHistoryProvider;
        private readonly IPriceRegistry _priceRegistry;

        public ReportsController(ISubscriptionResolver subscriptionResolver, IRequestHistoryProvider requestHistoryProvider, IPriceRegistry priceRegistry)
        {
            _subscriptionResolver = subscriptionResolver;
            _requestHistoryProvider = requestHistoryProvider;
            _priceRegistry = priceRegistry;
        }

        [HttpGet("{subscriptionID}/{year}/{month}")]
        public async Task<ActionResult<Report>> Get(string subscriptionID, int year, int month)
        {
            if (year < 1996 || year > DateTime.UtcNow.Year)
                return BadRequest(new Error {Message = "invalid year"});

            if (month < 1 || month > 12)
                return BadRequest(new Error {Message = "invalid month"});

            var userIdentifier = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            Log.Debug("Resolving subscriptions for User ID:{0}", userIdentifier);

            var subscriptions = await _subscriptionResolver.GetSubscriptions(userIdentifier);
            if (!subscriptions.Contains(subscriptionID))
                return Forbid(User.Identity.AuthenticationType);

            Log.Debug("Requesting history data for Year:{0} Month:{1} and Subscription ID:{2}", year, month, subscriptionID);
            var requestHistory = await _requestHistoryProvider.GetData(year, month, subscriptionID);

            // NOTE: as soon as we have no info about subscription we can't say whether the subscription already exists
            //       if request history is empty. So let's consider that subscription is always there and we return
            //       an empty report if no history found

            Log.Debug("Calculating prices for {0} requests", requestHistory.Length);
            var requestsByService = requestHistory
                .GroupBy(x => x.ServiceName)
                .ToDictionary(x => x.Key, x => x);

            var pricePerService = new Dictionary<string, double>();
            foreach (var serviceName in requestsByService.Keys)
                pricePerService[serviceName] = await _priceRegistry.GetPrice(serviceName, requestsByService[serviceName].Count());
            var totalPrice = pricePerService.Values.Sum();

            double? estimation = null;
            if (DateTime.UtcNow.Year == year && DateTime.UtcNow.Month == month)
            {
                Log.Debug("Preparing cost estimations by the end of the month");
                estimation = (double) DateTime.DaysInMonth(year, month) / DateTime.UtcNow.Day * totalPrice;
            }

            Log.Debug("Building report");
            return new Report
            {
                SubscriptionID = subscriptionID,
                Period = new Period
                {
                    From = new DateTime(year, month, 01),
                    To = new DateTime(year, month, DateTime.DaysInMonth(year, month)),
                },
                TotalRequests = requestHistory.Length,
                TotalPrice = totalPrice,
                TotalPerService = pricePerService.Select(x => new PerService { Name = x.Key, TotalPrice = x.Value }).ToArray(),
                Estimation = estimation,
            };
        }
    }
}
